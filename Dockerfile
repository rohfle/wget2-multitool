FROM registry.gitlab.com/rohfle/wget2-alpine:master

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ADD ./app /usr/src/app

CMD ["/bin/bash", "./start.sh"]
