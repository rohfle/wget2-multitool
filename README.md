# wget2-multitool

A docker-based multi-configuration download client using (wget2)[https://gitlab.com/gnuwget/wget2].

**NB this is just a prototype - expect that names of folders may change**

## How to use example

```
docker run -d -v /path/to/downloads:/download -e DOWNLOAD_DIR=/download
sleep 2
cd /path/to/downloads
cd .wget-multitool
cp -R example.disabled example
echo https://example.com/ > example/config/urls
echo mirror = on > example/config/wget2rc
# wait a few minutes
cd /path/to/downloads/example
tree
```

Output

```
example.com/
└── example.com
    └── index.html

1 directory, 1 file
```

## Features

- Site-specific retrieval interval
- Keep track of the last time a site was retrieved
- Site-specific wget2 config file
- Default configuration shared across all sites
- Site-specific logs

## Environment variables

- DOWNLOAD_DIR: where files will be downloaded to

## Wget2 settings set by wget2-multitool

```
--input-file         (set to config/urls)
--config             (set to state/wget2rc.combined)
--append-output      (set to logs/output.log)
--directory-prefix   (set to $DOWNLOAD_DIR/$SITE_NAME)
```

## Folder structure

When the container starts, the folder `.wget2-multitool/` is created in the
download directory. Inside there will be several subdirectories

- defaults/ - Default configuration that is shared between sites
- example.disabled/ - A template directory that can be copied to create a new site

If you rename the site to have the suffix `.disabled`, the site will not be retrieved.
Any other folder in this directory is considered a site folder and will be retrieved.

### The defaults folder

In `defaults` you will find:

- interval - default number of minutes between each retrieval of a site
- wget2rc - base configuration of wget2 that can be extended by sites

Any missing files and folders will be recreated during each loop

### Site folder

If each site folder (for example `example.disabled`) you will find:

- config/
  - urls - the urls to retrieve (handed to --input-file argument)
  - interval - site-specific number of minutes between each retrieval
  - wget2rc - site-specific overrides / extra settings for wget2
- logs/
  - output.log - the output from wget2 for the site
- state/
  - last_retrieved - a timestamp of when the last retrieval of site occurred
  - wget2rc.combined - a generated file combining default and site wget2rc files

Any missing files and folders will be recreated during each loop

### Licence

See https://github.com/darnir/wget2#license for Wget2 licenses.


Copyright 2019 Rohan Fletcher

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
