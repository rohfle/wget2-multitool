#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
BASE_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

function init_defaults_dir {
  DEFAULTS_DIR="$1"
  mkdir -p "$DEFAULTS_DIR"

  if [ ! -e "$DEFAULTS_DIR/interval" ]; then
    echo 5 > "$DEFAULTS_DIR/interval"
  fi
  if [ ! -e "$DEFAULTS_DIR/wget2rc" ]; then
    cat > "$DEFAULTS_DIR/wget2rc" << EOT
random-wait = on
wait = 1
force-directories = on
cut-url-get-vars = on
cut-file-get-vars = on
mirror = on
no-parent = on
https-only = on
https-enforce = hard
user-agent = wget2-multitool 0.1
max-threads = 4
no-robots = on
continue = on
limit-rate = 2m
EOT
  fi

}

function init_site_dir {
  SITE_DIR="$1"
  DEFAULTS_DIR="$2"
  # refresh site folder
  mkdir -p "$SITE_DIR/config"
  mkdir -p "$SITE_DIR/logs"
  mkdir -p "$SITE_DIR/state"
  # only overwrite interval file that doesn't exist
  if [ ! -e "$SITE_DIR/config/interval" ]; then
    cat "$DEFAULTS_DIR/interval" > "$SITE_DIR/config/interval"
  fi
  # make sure that urls and the site-specific wget2rc files are present
  if [ ! -e "$SITE_DIR/config/urls" ]; then
    touch "$SITE_DIR/config/urls"
  fi
  if [ ! -e "$SITE_DIR/config/wget2rc" ]; then
    cat > "$SITE_DIR/config/wget2rc" << EOT
## Other useful settings
# reject = .htaccess,_index.html
# default-page = _index.html
# http-user=user
# http-password=password
# limit-rate = 4m
# cut-dirs = 2
# no-host-directories = on
EOT
  fi

}


if [ ! -z "$DOWNLOAD_DIR" ]; then
  # env var set already - do nothing
  echo "Using DOWNLOAD_DIR env var as download directory"
elif [ "$#" -gt 0 ]; then
  DOWNLOAD_DIR="$1"
  echo "Using first argument as download directory"
else
  echo "Error: Download directory not set."
  echo "Either pass as first argument or set DOWNLOAD_DIR env var"
  exit 1
fi

if [ ! -d "$DOWNLOAD_DIR" ]; then
  echo "Error: Download directory '$DOWNLOAD_DIR' does not exist!"
  exit 1
else
  echo "Using download directory '$DOWNLOAD_DIR'"
fi

CONFIG_DIR="$DOWNLOAD_DIR/.wget2-multitool"
DEFAULTS_DIR="$CONFIG_DIR/defaults"
MIN_INTERVAL=5 # 5 minutes

mkdir -p "$CONFIG_DIR"
init_defaults_dir "$DEFAULTS_DIR"
init_site_dir "$CONFIG_DIR/example.disabled" $DEFAULTS_DIR

while true; do
  # refresh .wget2mirror folder
  mkdir -p "$CONFIG_DIR"
  # only overwrite files that don't exist
  init_defaults_dir "$DEFAULTS_DIR"

  let SITES_PROCESSED=0
  # iterate through site folders
  for SITE_DIR in "$CONFIG_DIR"/*/; do
    SITE_DIR="${SITE_DIR%*/}"
    SITE_NAME="$(basename "$SITE_DIR")"
    # ignore non-folders and the defaults folder
    if [[ ! -d "$SITE_DIR" || "$SITE_NAME" == "defaults" || "$SITE_NAME" == "example.disabled" ]]; then
      continue
    fi
    # ignore example folders or folders that are disabled
    if [[ "$SITE_NAME" == *".disabled" ]]; then
      echo "Ignoring site $SITE_NAME..."
      continue
    fi

    init_site_dir "$SITE_DIR" "$DEFAULTS_DIR"

    let SITES_PROCESSED=$(( $SITES_PROCESSED + 1 ))

    # compare last retrieved time and interval
    let INTERVAL=$(cat "$SITE_DIR/config/interval")
    if [[ "$INTERVAL" -lt "$MIN_INTERVAL" ]]; then
      let INTERVAL=$MIN_INTERVAL
    fi

    ##### TIME CHECKING #####
    # load time
    LAST_RETRIEVED="$(cat "$SITE_DIR/state/last_retrieved")"
    LAST_RETRIEVED_SEC=$(date -u +%s -D '%Y-%m-%dT%H:%M:%S' -d "$LAST_RETRIEVED")
    if [ $? -ne 0 ]; then
      echo "Last retrieved time missing or invalid. Forcing update..."
    else
      let LAST_RETRIEVED_SEC=$LAST_RETRIEVED_SEC
      let NOW_SEC=$(date -u +%s)
      let NEXT_SEC=$(( 60 * $INTERVAL + $LAST_RETRIEVED_SEC ))
      if [ $NOW_SEC -lt $NEXT_SEC ]; then
        echo "Next retrieve time still in future. Skipping $SITE_NAME..."
        continue
      else
        echo "Next retrieve time has passed. Getting $SITE_NAME..."
      fi
    fi

    # combine config only if it has changed
    if [ ! -f "$SITE_DIR/state/wget2rc.combined" ] || \
       [ "$CONFIG_DIR/defaults/wget2rc" -nt "$SITE_DIR/state/wget2rc.combined" ] || \
       [ "$SITE_DIR/config/wget2rc" -nt "$SITE_DIR/state/wget2rc.combined" ]; then
      echo "Config for $SITE_NAME changed - reloading..."
      cp "$CONFIG_DIR/defaults/wget2rc" "$SITE_DIR/state/wget2rc.combined"
      echo >> "$SITE_DIR/state/wget2rc.combined"
      cat "$SITE_DIR/config/wget2rc" >> "$SITE_DIR/state/wget2rc.combined"
      echo >> "$SITE_DIR/state/wget2rc.combined"
    fi

    wget2 \
      --input-file="$SITE_DIR/config/urls" \
      --config="$SITE_DIR/state/wget2rc.combined" \
      --append-output="$SITE_DIR/logs/output.log" \
      --directory-prefix="$DOWNLOAD_DIR/$SITE_NAME"

    if [ $? -eq 0 ]; then
      echo "Retrieval of $SITE_NAME has finished successfully."
      date -u -Iseconds > "$SITE_DIR/state/last_retrieved"
    else
      echo "Retrieval of $SITE_NAME returned an error."
    fi
  done
  echo "Finished run. $SITES_PROCESSED sites processed. Sleeping..."
  sleep 60
done
